import { Injectable } from '@angular/core';

@Injectable()
export class ModalServices{
  constructor(){}

  setFocusInput(idElement){
    setTimeout(()=>{
      document.getElementById(idElement).focus();
    },100);
  }

  scrollToTop(idElement){
    let myDiv = document.getElementById(idElement);
    myDiv.scrollTop = 0;
  }

}

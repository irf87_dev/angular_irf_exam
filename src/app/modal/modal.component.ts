import { Component, OnInit, OnChanges, Input, Output, EventEmitter, ViewChild } from '@angular/core';

//Custom imports
import { ModalServices } from './modal.service';
import { Persona} from '../model/persona';

declare var $:any;
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
  providers : [ModalServices]
})
export class ModalComponent implements OnInit {
  @ViewChild('modalForm') modalForm;

  bModalInit : boolean = false;
  confirmPassword : string = "";
  idModal = "#modalRegistros";
  isCreatingMode : boolean = true;
  oPersona : Persona = new Persona();

  @Input() showModal:boolean;
  @Input() data:any;

  @Output() onCloseModal = new EventEmitter();
  @Output() onApply = new EventEmitter();

  constructor(
    private modalService : ModalServices
  ) { }

  ngOnInit() {
    $(this.idModal).modal({ dismissible: false });
    this.bModalInit = true;
  }

  ngOnChanges(changes){
    //Open / close modal
    if(this.bModalInit){
      if(changes.showModal){
        if(changes.showModal.currentValue == true){
          if(this.modalForm) this.modalForm.resetForm({});
          $(this.idModal).modal('open');
          setTimeout(()=>{
            this.modalService.scrollToTop(this.idModal);
            this.modalService.setFocusInput("nombrePersona");
          },50);
        }
        if(changes.showModal.currentValue == false){
          this.cleanModal();
          $(this.idModal).modal('close');
        }
      }
    }

    //Data changes
    if(changes.data){
      if(changes.data.currentValue){
        this.detectMode(changes.data.currentValue);
      }
    }
  }

  //Custom Events
  apply(){
    this.onApply.emit(this.oPersona);
  }

  cleanModal(){
    this.isCreatingMode = true;
    this.oPersona = new Persona();
  }

  closeModal(){
    this.onCloseModal.emit();
  }

  //Private functions

  detectMode(data){
    if(data.nombre){
      this.oPersona = this.data;
      this.isCreatingMode = true;
      setTimeout(()=>{
        this.isCreatingMode = false;
        this.confirmPassword = this.data.password;
      },100);
    }
    else{
      this.oPersona = new Persona();
      this.isCreatingMode = true;
      this.confirmPassword = undefined;
    }
  }

}

import { Directive, Input, OnChanges, SimpleChanges, ElementRef } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

/**custom regular expression */
export function forbiddenNameValidator(nameRe: RegExp, isRequired : boolean): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    let forbidden = true;
    if(isRequired){
      forbidden = nameRe.test(control.value);
    }
    else{
      if(control.value){
        forbidden = nameRe.test(control.value);
      }
    }
    if(forbidden) return null;
    else return {'custonValidation' : forbidden};
  };
}

@Directive({
  selector: '[appForbiddenName]',
  providers: [{provide: NG_VALIDATORS, useExisting: ForbiddenValidatorDirective, multi: true}]
})
export class ForbiddenValidatorDirective implements Validator {
  public isRequired : boolean = false;
  @Input('appForbiddenName') forbiddenName: string;

  constructor(private el: ElementRef) { }

  validate(control: AbstractControl): {[key: string]: any} | null {

    return this.forbiddenName ? forbiddenNameValidator(new RegExp(this.forbiddenName),this.isRequired)(control)
                              : null;
  }
  ngAfterViewInit() {
    this.isRequired = this.el.nativeElement.required;
  }
}

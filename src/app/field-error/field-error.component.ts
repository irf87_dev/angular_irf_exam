import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Error } from './error-msg';

@Component({
  selector: 'app-field-error',
  templateUrl: './field-error.component.html',
  styleUrls: ['./field-error.component.css']
})
export class FieldErrorComponent implements OnInit {
  error: Error = new Error();
  errorMsg: string = "";
  showCustomError: boolean = false;

  @Input() errorMsgCustom: string = "";
  @Input() show: boolean = false;
  @Input() validatorError: any = {};

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    if (changes.validatorError) {
      if (changes.validatorError) {
        this.errorMsg = this.error.getMsg(this.getKeyValue(changes.validatorError.currentValue));
        this.errorMsg += this.extraInfo(changes.validatorError.currentValue);
      }
    }
  }

  getKeyValue(object) {
    let key = "";
    if (object) {
      let keyAux = Object.keys(object);
      if (keyAux.length > 0) key = keyAux[0];
    }
    return key;
  }

  extraInfo(object) {
    let msg = "";
    let type = this.getKeyValue(object);
    this.showCustomError = false;
    switch (type) {
      case 'minlength':
        msg = String(object[type].requiredLength)
      break;

      case 'custonValidation':
        this.showCustomError = !object["custonValidation"];
      break;
    }
    return msg;
  }

}

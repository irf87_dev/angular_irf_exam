export class Error {
  private cat_error: any = {

    "required": {
      "message": "Campo requerido"
    },
    "minlength": {
      "message": "El tamaño minimo es de "
    },
    "email": {
      "message" : "El formato de email es incorrecto"
    }
  };

  constructor() {
  }

  getMsg(type) {
    let msg = "";
    if (type) {
      if (this.cat_error) {
        if (this.cat_error[type]) {
          msg = this.cat_error[type].message;
        }
      }
    }
    return msg;
  }

}

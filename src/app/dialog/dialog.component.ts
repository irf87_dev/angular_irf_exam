import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  idModal = "#dialogo";

  @Input() boton: string = "";
  @Input() show: boolean = false;
  @Input() titulo: string = "";
  @Input() texto: string = "";
  @Input() tipo: string = "";
  /*
    Código de tipos:
    critial = rojo
    warning = amber
    default = blanco
  */

  @Output() evtAccion = new EventEmitter();

  constructor() { }

  ngOnInit() {
    $(this.idModal).modal({ dismissible: false });
  }

  ngOnChanges(changes) {
    if (changes.show) {
      if (changes.show.currentValue == true) this.open();
    }
  }

  //Custom Events
  open() {
    $(this.idModal).modal('open');
  }

  close(accion) {
    this.evtAccion.emit(accion);
    $(this.idModal).modal('close');
  }

}

export class Persona {
  active : boolean;
  nombre : string;
  apellidos : string;
  correo : string;
  password : string;

  constructor(){
    this.active = true;
  }
}

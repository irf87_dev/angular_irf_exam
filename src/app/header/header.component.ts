import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  search : string = "";
  showSearchBar : boolean = false;

  @Input() searchInput : boolean = false;

  @Output() onSearch = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  //Custom Events
  onClearSearch(){
    this.search = "";
  }

  onEnterSearch(){
    this.onSearch.emit(this.search);
  }

  searchNavHanddler(){
    this.showSearchBar = !this.showSearchBar;
  }

}

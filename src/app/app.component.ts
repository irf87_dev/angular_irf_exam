import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  arrayPersonas : any = [];
  currentRecord : any;
  currentIndex : number;
  title = 'examangular';
  search: string = "";
  showModal : boolean = false;


  /*
    VARIABLES DIALOGO
  */
  showDialog: boolean = false;
  titleDialog: string = "Eliminar registro";
  txtDialog: string = "";

  //Custom Events

  addRecord(){
    this.showModal = true;
  }

  apply(oPersona){
    if(this.currentIndex >= 0){
      this.arrayPersonas[this.currentIndex] = oPersona;
    }
    else{
      this.arrayPersonas.push(oPersona);
    }
    this.closeModal();
  }

  askToDeleteRegistry(index, data ){
    this.currentRecord = data;
    this.currentIndex  = index;
    this.showDialog = true;
    this.txtDialog = "¿Esta seguro de eliminar el registro ";
    this.txtDialog += data.nombre + " " + data.apellidos + " ?";
  }

  closeModal(){
    this.clean();
    this.showModal = false;
  }

  dialogEvt(accion){
    this.showDialog = false;
    if(accion == 1) this.deleteItem();
  }

  editRecord(index, data){
    //Eliminar Inmutabilidad
    let oAux = JSON.stringify(data);
    oAux = JSON.parse(oAux);

    this.currentIndex = index;
    this.currentRecord = oAux;
    this.showModal = true;
  }

  searching(txtSearch) {
    this.search = txtSearch;
  }

  //Private funcions

  clean(){
    this.currentIndex = undefined;
    this.currentRecord = undefined;
  }

  deleteItem(){
    this.arrayPersonas.splice(this.currentIndex,1);
    this.clean();
  }

}
